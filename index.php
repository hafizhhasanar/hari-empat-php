<?php

    require("animal.php");
    require("ape.php");
    require("frog.php");

    $sheep = new Animal("shaun");
    echo "Name : " .$sheep->name . "<br>" ;
    echo "Legs : " .$sheep->legs . "<br>" ;
    echo "Cold Blooded : " .$sheep->cold_bloonded . "<br>" ;
    echo "<br>" ;

    $sungokong = new Ape("Kera Sakti");
    echo "Name : " .$sungokong->name . "<br>" ;
    echo "Legs : " .$sungokong->legs . "<br>" ;
    echo "Cold Blooded : " .$sungokong->cold_bloonded . "<br>" ;
    echo $sungokong->yell("Auooo"). "<br>";
    echo "<br>";

    $kodok = new Frog("Buduk");
    echo "Name : " .$kodok->name . "<br>" ;
    echo "Legs : " .$kodok->legs . "<br>" ;
    echo "Cold Blooded : " .$kodok->cold_bloonded . "<br>" ;
    echo $kodok->jump("hop hop"). "<br>";


?>
